## 📞 You've reached my README! 

![Alt text](https://media.giphy.com/media/3oKIPsx2VAYAgEHC12/giphy.gif)

I created this to help you get to know me, how I work, why I enjoy working with others, and hopefully somewhere along the way, help build some comfort in wanting to connect. 

**🦊 GitLab handle:** `@gdoyle`

**🖥️ LinkedIn:** https://www.linkedin.com/in/gdoyle25/

_This README was inspired by [Austin Regnery's](https://gitlab.com/aregnery)._

## Hi, I'm Gina 👋 👩🏻

**Some personal things...**

- I use she/her pronouns.
- I currently live in Uxbridge, MA with my partner. No dog yet, but there will be one soon 🐶!
- I've been in 🇺🇸 Massachusetts my whole life, as I grew up in Hopkinton, and then studied [Human Factors Engineering](https://asegrad.tufts.edu/program/human-factors-engineering-masters) (also called Engineering Psychology) at [Tufts University](https://www.tufts.edu/). I also played on the Women's basketball team at Tufts 🏀. 👉 Fun fact: I played against one of [Randy Moss's](https://en.wikipedia.org/wiki/Randy_Moss) daughters at the 2016 NCAA championship 🍾.
- I enjoy staying active still, usually doing some form of high intensity interval training or strength training. Most recently, I joined a [CrossFit](https://www.crossfit.com/) gym 🏋🏻.

**Now onto work...**

- I'm a Senior Product Designer for the [Runner group](https://handbook.gitlab.com/handbook/product/categories/#runner-group) 🏃 (specifically the Fleet team). I joined GitLab in July of 2021.
- [Collaboration](https://handbook.gitlab.com/handbook/values/#collaboration) 🤝 has always been a priority for me at work. It's something I learned from playing sports with a team all my life. You'll see that in a lot of the work I do, such as [sharing designs early and often](https://handbook.gitlab.com/handbook/product/ux/product-designer/#priorities), leading the [Tufts University capstone project](https://handbook.gitlab.com/handbook/product/ux/learning-and-development/tufts-university-capstone-course/) for the UX department, and consistently asking for [feedback](https://handbook.gitlab.com/handbook/company/culture/all-remote/effective-communication/#feedback-is-a-gift) and making time to give others feedback if they ask.
- I'm familiar with designing for developers with technical use cases because of my previous work at Red Hat where I worked on [OpenShift](https://www.redhat.com/en/technologies/cloud-computing/openshift). If you have questions about Runner, reach out! 🛑 **{-Beware-}**: I'm certainly not an expert, I may not know the answer, but I can point you to those who are 😊.
- I really enjoy mentoring students who are interested in UX, no matter the age. I've had experience running UX programs with [high school students](https://medium.com/@gina.doyle2442/design-in-technology-a-6-week-workshop-for-high-schoolers-bf0f80140517) as well as [college students](https://www.linkedin.com/posts/gdoyle25_tufts-human-factors-society-on-instagram-activity-7121169246633517057-t5Y7?utm_source=share&utm_medium=member_desktop).

## Personal principles
- The outcome should always result in helping someone.
- Other ideas to solve problems are not just welcomed, they're seeked. Same with feedback!
- Set goals that are realistic, but be relentless to succeed.
- Quality over quantity.
- When it comes to pitching an idea, _bring the data_.
- Accept failures and mistakes, learn from them, and use them to be better.

## My working style
- I prefer to work [async-first](https://handbook.gitlab.com/handbook/company/culture/all-remote/asynchronous/) always, unless there is a topic to brainstorm on, where I'll typically reach out to team members and see if they'd be comfortable with a sync session.
- I take pride in my organization skills, and therefore am almost always editing issue descriptions, organizing epics, and updating my [priorities list](https://gitlab.com/gdoyle/plan/-/blob/main/2024-tasks.md). I'm impatient when it comes to cleaning up issues, but I will make sure to loop DRIs in when I do. 📝 Please help me with organizational efforts!
- When a new problem appears, I always start with data. Even if one customer meeting talks about an issue, I want to make sure that problem is validated before moving it through the [product development flow](https://handbook.gitlab.com/handbook/product-development-flow/).
- A big reason why I joined GitLab was because of the opensource handbook, documented processes, and collaboration and contribution practices. Because of that, I am a stickler for process. The work I take on will always have a documented process at it's core. With that said, if something isn't working, tell me! I am open to change if there's enough data to back it up (are you seeing the trend yet? 📈).


👾 This page is not meant to be static, I'll continue to iterate on it as my career progresses. 
